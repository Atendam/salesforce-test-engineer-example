Coding exercise for Salesforce. 
Steps to run tests.
1. Open Command prompt(windows)/Terminal(linux)
2. cd into directory where the repository was cloned.
3. Run command  "ruby web_form_test.rb"

Conclusion:
    As I was writing my positive and negative test for required field validation. 
I noticed that the lack of unique identifiers on the web elements poised a problem for reliability 
of the testing framework. Having unique identifiers on every expected element within the form helps
to make the test suite, and and the product more rigid. Having these things help formulate
concrete tests that will never fail based on an element movement within the page. 
Having to rely on XPATH to find elements would make me question the product stability, 
along with how much is already in production for the given product. By adding unique identifiers it will 
allow for a stronger test suite and alleviate the potential cause of defects found in production.