require 'rubygems'
require 'selenium-webdriver'
require 'test/unit'

class WebFormTest < Test::Unit::TestCase
  def setup
    #creates the browser object and opens the browser
    @driver = Selenium::WebDriver.for :chrome
    @wait = Selenium::WebDriver::Wait.new :timeout => 10
    @driver.get 'https://docs.google.com/forms/d/181whJlBduFo5qtDbxkBDWHjNQML5RutvHWOCjEFWswY/viewform'
  end

  # This test is written to assert that the error messages for the required fields are not displayed to the user when
  # a correct value is filled for that specific field.
  #
  def test_required_field_validations_no_errors
    @driver.find_element(:name => 'entry.1041466219').send_keys 'Andrew'
    @driver.find_element(:name => 'entry.310473641').click
    assert_false(@driver.find_element(:xpath => '//*[@id="ss-form"]/ol/div[1]/div/div/div[2]' ).displayed?)
    assert_false(@driver.find_element(:xpath => '//*[@id="ss-form"]/ol/div[2]/div/div/div[2]' ).displayed?)
  end

  # Test required field validation errors asserts that the error messages for the required form fields are displayed
  # to the user to indicate the form fields need to be filled

  def test_required_field_validation_errors
    @driver.find_element(:name => 'entry.1041466219').send_keys :tab
    assert_true(@driver.find_element(:xpath => '//*[@id="ss-form"]/ol/div[1]/div/div/div[2]' ).displayed?)
    @driver.find_element(:name => 'submit').click
    assert_true(@driver.find_element(:xpath => '//*[@id="ss-form"]/ol/div[2]/div/div/div[2]' ).displayed?)
  end

  def teardown
    @driver.quit
  end
end